﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MoviesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly Services.IGenresService _genresService;
        public GenresController(Services.IGenresService genresService)
        {
            _genresService = genresService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllGenres()
        {
            var genres = await _genresService.GetAllGenres();

            return Ok(genres);
        }

        [HttpPost]
        public async Task<IActionResult> AddGenres(CreateGenreDto dto)
        {
            var genre = new Genre { Name = dto.Name };
            await _genresService.AddGenre(genre);
            return Ok(genre);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateGenre(byte id,[FromBody] CreateGenreDto dto)
        {
            var genre = await _genresService.GetGenreById(id);
            if(genre == null) 
             return NotFound($"No genre was found with ID:{id}"); 
            genre.Name = dto.Name;
           _genresService.UpdateGenre(genre);
            return Ok(genre);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGenre(byte id)
        {
            var genre = await _genresService.GetGenreById(id);
            if (genre == null)
                return NotFound($"No genre was found with ID:{id}");
           _genresService.DeleteGenre(genre);
            return Ok(genre);
        }
    }
}
