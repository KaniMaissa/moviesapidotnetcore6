﻿namespace MoviesApi.Services
{
    public interface IMoviesService
    {
        Task <IEnumerable<Movie>> GetAll (byte genreId = 0);
        Task <Movie> GetById (int id);
        Task<Movie> GetByGenreIdAsync(byte genreId);
        Task<Movie> AddMovie (Movie movie);
        Movie UpdateMovie (Movie movie);
        Movie DeleteMovie (Movie movie);
    }
}
