﻿namespace MoviesApi.Services
{
    public interface IGenresService
    {
        Task<IEnumerable<Genre>> GetAllGenres();
        Task <Genre> GetGenreById(byte id);
        Task <Genre> AddGenre(Genre genre);
        Genre UpdateGenre(Genre genre);
        Genre DeleteGenre(Genre genre);
        Task <bool> IsvalidGenre(byte id);
    }
}
